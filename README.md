# L'université des sorciers

Une API pour récupérer des infos sur l'université des sorciers

## Utiliser l'API

Les routes sont configurées comme suit :

Pour récupérer toutes les entités d'une ressource, l'url doit être construite sur ce modèle :  
`{url}/api/{type de ressource}`

Il est également possible de récupérer une entité spécifique en précisant son index :  
`{url}/api/{type de ressource}/{id}`

**Exemple :**  
`http://127.0.0.1:8000/api/students/1`

6 types de ressources sont disponibles à la consultation :
- `students`
- `programs`
- `courses`
- `teachers`
- `sessions`
- `rooms`


## Développer l'API

### Pour commencer

L'API est construite avec Laravel et utilise MySQL pour la base de données. Des données de test sont disponibles dans les seeds. Pour commencer, clonez ce dépôt :

```sh
git clone git@gitlab.com:timotheesterle/universite_sorciers.git
```

Installez les paquets nécessaires :

```sh
composer install
```

Pour créer le modèle de base de données en local et récupérer les données de test, veillez à créer un fichier `.env` basé sur le fichier `.env.example` dans le dossier du projet et à remplir les champs `DB_DATABASE`, `DB_USERNAME` et `DB_PASSWORD` avec vos propres identifiants. Ensuite, lancez la commande :

```sh
php artisan migrate --seed
```

Lancez le serveur :

```sh
php artisan serve
```

### Schémas de base de données

Le dossier `diagrams` contient la modélisation de la BDD sous format `.png` et `.dbml` (à utiliser sur https://dbdiagram.io/).
