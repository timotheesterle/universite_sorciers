<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
    protected $fillable = [
        'intitule',
    ];

    public function cours() {
        return $this->belongsToMany('App\Cours');
    }

    public function eleves() {
        return $this->hasMany('App\Eleve');
    }
}
