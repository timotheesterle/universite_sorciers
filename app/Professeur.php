<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professeur extends Model
{
    protected $fillable = [
        'nom',
        'prenom',
        'email',
        'statut',
    ];

    public function cours() {
        return $this->belongsToMany('App\Cours');
    }
}
