<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
    protected $fillable = [
        'nom'
    ];

    public function cours() {
        return $this->belongsToMany('App\Cours', 'seances');
    }
}
