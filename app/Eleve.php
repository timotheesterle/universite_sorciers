<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eleve extends Model
{
    protected $fillable = [
        'nom',
        'prenom',
        'adresse',
        'date_naissance',
        'date_inscription',
        'programme_id',
    ];

    public function programmes() {
        return $this->belongsTo('App\Programme');
    }
}
