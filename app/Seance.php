<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seance extends Model
{
    protected $fillable = [
        'jour_de_la_semaine',
        'horaire',
        'salle_id',
        'cours_id',
    ];
}
