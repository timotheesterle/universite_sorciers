<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Salle as SalleResource;
use App\Salle;
use App\Http\Resources\Cours as CoursResource;
use App\Cours;

class Seance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'jour_de_la_semaine' => $this->jour_de_la_semaine,
            'horaire' => $this->horaire,
            'salle' => new SalleResource(Salle::find($this->salle_id)),
            'cours' => new CoursResource(Cours::find($this->cours_id)),
        ];
    }
}
