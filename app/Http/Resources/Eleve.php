<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Programme as ProgrammeResource;
use App\Programme;

class Eleve extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'prenom' => $this->prenom,
            'adresse' => $this->adresse,
            'date_naissance' => $this->date_naissance,
            'date_inscription' => $this->date_inscription,
            'programme' => new ProgrammeResource(Programme::find($this->programme_id)),
        ];
    }
}
