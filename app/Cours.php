<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cours extends Model
{
    protected $fillable = [
        'intitule',
        'duree',
    ];

    public function programmes() {
        return $this->belongsToMany('App\Programme');
    }

    public function professeurs() {
        return $this->belongsToMany('App\Professeur');
    }

    public function salles() {
        return $this->belongsToMany('App\Salle', 'seances');
    }
}
