<?php

use Illuminate\Http\Request;
use App\Http\Resources\Eleve as EleveResource;
use App\Eleve;
use App\Http\Resources\Professeur as ProfesseurResource;
use App\Professeur;
use App\Http\Resources\Cours as CoursResource;
use App\Cours;
use App\Http\Resources\Programme as ProgrammeResource;
use App\Programme;
use App\Http\Resources\Seance as SeanceResource;
use App\Seance;
use App\Http\Resources\Salle as SalleResource;
use App\Salle;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/students', function() {
    return EleveResource::collection(Eleve::all());
});

Route::get('/students/{id}', function($id) {
    return new EleveResource(Eleve::find($id));
});

Route::get('/teachers', function() {
    return ProfesseurResource::collection(Professeur::all());
});

Route::get('/teachers/{id}', function($id) {
    return new ProfesseurResource(Professeur::find($id));
});

Route::get('/courses', function() {
    return CoursResource::collection(Cours::all());
});

Route::get('/courses/{id}', function($id) {
    return new CoursResource(Cours::find($id));
});

Route::get('/programs', function() {
    return ProgrammeResource::collection(Programme::all());
});

Route::get('/programs/{id}', function($id) {
    return new ProgrammeResource(Programme::find($id));
});

Route::get('/sessions', function() {
    return SeanceResource::collection(Seance::all());
});

Route::get('/sessions/{id}', function($id) {
    return new SeanceResource(Programme::find($id));
});

Route::get('/rooms', function() {
    return SalleResource::collection(Salle::all());
});

Route::get('/rooms/{id}', function($id) {
    return new SalleResource(Salle::find($id));
});
