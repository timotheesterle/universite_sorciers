<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProfesseursSeeder::class);
        $this->call(SallesSeeder::class);
        $this->call(CoursSeeder::class);
        $this->call(ProgrammesSeeder::class);
        $this->call(ElevesSeeder::class);
    }
}
