<?php

use Illuminate\Database\Seeder;
use App\Professeur;

class ProfesseursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dumbledore = Professeur::create([
            'nom' => 'Dumbledore',
            'prenom' => 'Albus',
            'email' => 'a.dumbledore@hogwarts.uk',
            'statut' => 'salarie',
        ]);

        $rogue = Professeur::create([
            'nom' => 'Rogue',
            'prenom' => 'Severus',
            'email' => 's.rogue@hogwarts.uk',
            'statut' => 'salarie',
        ]);

        $mcgonagall = Professeur::create([
            'nom' => 'McGonagall',
            'prenom' => 'Minerva',
            'email' => 'm.mcgonagall@hogwarts.uk',
            'statut' => 'salarie',
        ]);
    }
}
