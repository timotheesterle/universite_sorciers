<?php

use App\Salle;
use Illuminate\Database\Seeder;

class SallesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $A1 = Salle::create([
            'nom' => 'A1',
        ]);

        $A2 = Salle::create([
            'nom' => 'A2',
        ]);

        $A3 = Salle::create([
            'nom' => 'A3',
        ]);

        $A4 = Salle::create([
            'nom' => 'A4',
        ]);

        $A5 = Salle::create([
            'nom' => 'A5',
        ]);

        $A6 = Salle::create([
            'nom' => 'A6',
        ]);

        $A7 = Salle::create([
            'nom' => 'A7',
        ]);

        $A8 = Salle::create([
            'nom' => 'A8',
        ]);

        $A9 = Salle::create([
            'nom' => 'A9',
        ]);

        $A10 = Salle::create([
            'nom' => 'A10',
        ]);

        $B1 = Salle::create([
            'nom' => 'B1',
        ]);

        $B2 = Salle::create([
            'nom' => 'B2',
        ]);

        $B3 = Salle::create([
            'nom' => 'B3',
        ]);

        $B4 = Salle::create([
            'nom' => 'B4',
        ]);

        $B5 = Salle::create([
            'nom' => 'B5',
        ]);

        $B6 = Salle::create([
            'nom' => 'B6',
        ]);
    }
}
