<?php

use Illuminate\Database\Seeder;
use App\Eleve;

class ElevesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $harry = Eleve::create([
            'nom' => 'Potter',
            'prenom' => 'Harry',
            'adresse' => '4 Private Drive',
            'date_naissance' => '1980-07-31',
            'date_inscription' => '1987-09-04',
            'programme_id' => 1,
        ]);

        $ron = Eleve::create([
            'nom' => 'Weasley',
            'prenom' => 'Ron',
            'adresse' => 'Le Terrier',
            'date_naissance' => '1980-03-01',
            'date_inscription' => '1987-09-04',
            'programme_id' => 2,
        ]);

        $hermione = Eleve::create([
            'nom' => 'Granger',
            'prenom' => 'Hermione',
            'adresse' => 'Hampstead, London',
            'date_naissance' => '1979-09-19',
            'date_inscription' => '1987-09-04',
            'programme_id' => 1,
        ]);

        $drago = Eleve::create([
            'nom' => 'Malefoy',
            'prenom' => 'Drago',
            'adresse' => 'Marly-Gomont',
            'date_naissance' => '1980-06-05',
            'date_inscription' => '1987-09-04',
            'programme_id' => 1,
        ]);

        $neville = Eleve::create([
            'nom' => 'Londubat',
            'prenom' => 'Neville',
            'adresse' => 'Levallois-Perret',
            'date_naissance' => '1980-07-30',
            'date_inscription' => '1987-09-04',
            'programme_id' => 1,
        ]);
    }
}
