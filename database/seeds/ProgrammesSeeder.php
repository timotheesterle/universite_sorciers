<?php

use Illuminate\Database\Seeder;
use App\Programme;

class ProgrammesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $magieblanche1 = Programme::create([
            'intitule' => 'Magie blanche 1ère année',
        ]);

        $magieblanche1->cours()->attach(1);

        $defense_magienoire1 = Programme::create([
            'intitule' => 'Défense contre la magie noire 1ère année',
        ]);

        $defense_magienoire1->cours()->attach(1);
        $defense_magienoire1->cours()->attach(2);
    }
}
