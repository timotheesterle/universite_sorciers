<?php

use Illuminate\Database\Seeder;
use App\Cours;

class CoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $potions = Cours::create([
            'intitule' => 'Potions',
            'duree' => '01:00:00',
        ]);

        $potions->professeurs()->attach(2);
        $potions->salles()->attach(5, ['jour_de_la_semaine' => 'mardi', 'horaire' => '14:00:00']);

        $defense_forces_mal = Cours::create([
            'intitule' => 'Défense contre les forces du mal',
            'duree' => '02:00:00',
        ]);

        $defense_forces_mal->professeurs()->attach(1);
        $defense_forces_mal->salles()->attach(3, ['jour_de_la_semaine' => 'lundi', 'horaire' => '10:00:00']);

        $levitation = Cours::create([
            'intitule' => 'Lévitation et annulation de gravité',
            'duree' => '01:00:00',
        ]);

        $levitation->professeurs()->attach(3);
        $levitation->salles()->attach(10, ['jour_de_la_semaine' => 'vendredi', 'horaire' => '16:00:00']);
    }
}
